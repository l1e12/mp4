package mp4;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

// TODO: You should implement suitable JUnit tests to verify that your implementation of the MovieGraph class is correct.

public class MovieGraphTest {

	int id1, id2, id3;
	int release;
	int weight1_2, weight1_3, weight2_3;
	String name1, name2, name3;
	String imdbUrl1, imdbUrl2, imdbUrl3;
	Movie m1, m2, m3;
	
	//ID of movie reviewed mapped to (reviewer ID mapped to score)
	Map<Integer, HashMap<Integer, Integer>> reviews = new HashMap<Integer, HashMap<Integer, Integer>>();
	
	@Before
	public void setup(){
		id1 = 1;
		id2 = 2;
		id3 = 3;
		
		release = 1995;
		
		name1 = "Toy Story";
		name2 = "GoldenEye";
		name3 = "Four Rooms";
		
		imdbUrl1 = "http://us.imdb.com/M/title-exact?Toy%20Story%20";
		imdbUrl2 = "http://us.imdb.com/M/title-exact?GoldenEye%20";
		imdbUrl3 = "http://us.imdb.com/M/title-exact?Four%20Rooms%20";
		
		m1 = new Movie(id1, name1, release, imdbUrl1);
		m2 = new Movie(id2, name2, release, imdbUrl2);
		m3 = new Movie(id3, name3, release, imdbUrl3);
		
		weight1_2 = 1;
		weight1_3 = 3;
		weight2_3 = 5;
	}
	
	@Test
	public void movieEqualsTest() {
		Movie m1_2 = new Movie(id1, name1, release, imdbUrl1);
		
		assertTrue(m1.equals(m1_2));
		assertFalse(m1.equals(m3));
	}
	
	@Test
	public void addVertexTest() {
		MovieGraph g = new MovieGraph();
				
		assertTrue(g.addVertex(m1));
		
		Set<Movie> movieSet = g.getVertecies();
		assertTrue(movieSet.contains(m1));
	}
	
	@Test
	public void addDuplicateTest(){
		MovieGraph g = new MovieGraph();
		
		g.addVertex(m1);
		
		assertFalse(g.addVertex(m1));
	}
	
	@Test
    public void addEdgeTest(){
		MovieGraph g = new MovieGraph();
		
		g.addVertex(m1);  //
		g.addVertex(m2);  //Add movies to graph g
		g.addVertex(m3);  //
		
		g.addEdge(m1, m2, weight1_2);  //
		g.addEdge(m1, m3, weight1_3);  //Add edges connecting all 3 movies
		g.addEdge(m2, m3, weight2_3);  // 
		
		assertFalse(g.addEdge(m2, m3, weight2_3)); //Cannot add duplicate edges
		
		//Check that the movies have bi-directional weights
		int w1to2 = g.getWeight(m1, m2);
		int w2to1 = g.getWeight(m2, m1);
		
		assertEquals(w1to2, weight1_2);
		assertEquals(w1to2, w2to1);
		
		//Check that multiple links can be stored and the have the correct values
		int w1to3 = g.getWeight(m1, m3);
		
		assertEquals(w1to2, weight1_2);
		assertEquals(w1to3, weight1_3);
	}
	
	@Test
	public void shortestPathTest(){ //Tests length and path
		MovieGraph g = new MovieGraph();
		
		g.addVertex(m1);  //
		g.addVertex(m2);  // Add movies to graph g
		g.addVertex(m3);  //
		
		g.addEdge(m1, m2, weight1_2);  //
		g.addEdge(m1, m3, weight1_3);  // Add edges connecting all 3 movies
		g.addEdge(m2, m3, weight2_3);  // 
		
		g.addVertex(new Movie(404, "Disconnected Movie", 404, "404"));
		
		//Test throwing NoPath
		try{
			g.getShortestPath(m2.getid(), 404);
			
			fail("Should throw NoPathException");
		}catch(NoPathException e){
			assertTrue(true);
		} catch (NoSuchMovieException e) {
			fail("Should not throw NoSuchMovieException");
		}
		
		//Test throwing NoSuchMovie
		try{
			g.getShortestPath(m2.getid(), -1); //try finding shortest path to nonexistant movie
			
			fail("Should throw NoSuchMovieException");
		}catch(NoPathException e){
			fail("Should not throw NoPathException");
		} catch (NoSuchMovieException e) {
			assertTrue(true);
		}
		
		List<Movie> foundPath = new ArrayList<Movie>();
		List<Movie> foundPathReverse = new ArrayList<Movie>();
		int foundLength = 0;
		List<Movie> shortestPath = new ArrayList<Movie>();
		
		//Test shortest path being direct between movies
		try {
			foundPath = g.getShortestPath(m1.getid(), m3.getid());
			foundPathReverse = g.getShortestPath(m3.getid(), m1.getid());
			foundLength = g.getShortestPathLength(m1.getid(), m3.getid());
		} catch (NoSuchMovieException | NoPathException e) {
			fail("Should not throw an exception");
		}
		
		shortestPath.add(m3); // correct path
		shortestPath.add(m1); //
		
		assertEquals(shortestPath, foundPath);
		shortestPath.remove(m3);
		shortestPath.add(m3);
		assertEquals(shortestPath, foundPathReverse);
		assertEquals(weight1_3, foundLength);
		
		//Test shortest path not directly between movies
		try {
			foundPath = g.getShortestPath(m2.getid(), m3.getid());
			foundPathReverse = g.getShortestPath(m3.getid(), m2.getid());
			foundLength = g.getShortestPathLength(m2.getid(), m3.getid());
		} catch (NoSuchMovieException | NoPathException e) {
			fail("Should not throw an exception");
		}
		
		shortestPath.clear(); //
		shortestPath.add(m3); //setup new correct path
		shortestPath.add(m1); //
		shortestPath.add(m2); //
		
		assertEquals(shortestPath, foundPath);
		shortestPath.remove(m1);
		shortestPath.add(m1);
		shortestPath.remove(m3);
		shortestPath.add(m3);
		assertEquals(shortestPath, foundPathReverse);
		assertEquals(weight1_2 + weight1_3, foundLength);
	}
	
	@Test
	public void testPathfindingWithData() throws IOException {
		MovieGraph g = new MovieGraph();

		MovieIterator iter = new MovieIterator("data/u.item.txt");
		
		while ( iter.hasNext() ) {
			Movie movie = iter.getNext();
			
			g.addVertex(movie);
		}
		
		//Builds list of ratings
		RatingIterator iter2 = new RatingIterator("data/u.data.txt");
		while (iter2.hasNext()) {
			Rating rating = iter2.getNext();
			
			if(reviews.containsKey(rating.getMovieId()))
				reviews.get(rating.getMovieId()).put(rating.getUserId(), rating.getRating());
			else{
				HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
				map.put(rating.getUserId(), rating.getRating());
				
				reviews.put(rating.getMovieId(), map);
			}
		}
		
		//adds weights between movies	
		for(Movie m1 : g.getVertecies()){
			for(Movie m2 : g.getVertecies()){
				if(!m2.equals(m1)){
					int edgeWeight = calcWeight(m1, m2);
				
					g.addEdge(m1, m2, edgeWeight);
				}
			}
		}			
		
		try {
			assertEquals(2, g.getShortestPathLength(215, 145));
			assertEquals(2, g.getShortestPathLength(145, 215));
		} catch (NoSuchMovieException | NoPathException e) {
			fail("Should not throw exceptions");
		}
	}
	public int calcWeight(Movie m1, Movie m2){
		Set<Integer> m1Reviewers = reviews.get(m1.getid()).keySet(); //
		Set<Integer> m2Reviewers = reviews.get(m2.getid()).keySet(); //reviewer IDs
		Set<Integer> totalRevs = new HashSet<Integer>();
		totalRevs.addAll(m1Reviewers);
		for(Integer i : m2Reviewers)
			if(!totalRevs.contains(i))
				totalRevs.add(i);

		int totalRev = totalRevs.size();
		int likerOverlap = 0;
		int dislikerOverlap = 0;
		
		for(Integer user : m2Reviewers){
			if(reviews.get(m1.getid()).containsKey(user)){
				int rev1 = reviews.get(m1.getid()).get(user);
				int rev2 = reviews.get(m2.getid()).get(user);
				
				if(rev1 > 3 && rev2 > 3)
					likerOverlap++;		
				else if(rev1 < 3 && rev2 < 3)
					dislikerOverlap++;	
			}
		}
		
		return (1 + totalRev - (likerOverlap + dislikerOverlap));
	}
	
	
}
