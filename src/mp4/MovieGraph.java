package mp4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

// The edges are weighted.
// This graph should be immutable except for the addition of vertices and edges. 
// It should not be possible to change a vertex after it has been added to the graph.

// You should indicate what the representation invariants and the abstraction function are for the representation you choose.
/**
 * 
 * @author Graham Greig & Dan Herdman
 * 
 * Rep Invariant:
 * Vertices are Movie objects 
 * Number of movies is >= 0
 * Edges connect exactly two movies
 * 
 * 
 * Abstraction Function:
 * A mapping between movies(vertices) based on a user provided edge weight.
 */
public class MovieGraph {
    
    final private Map<String,Integer> movieMap = new HashMap<String,Integer>();
    final private Map<Integer,Movie> IDMap = new HashMap<Integer,Movie>();
	
    final private Map<Movie, HashMap<Movie, Integer>> movies = new HashMap<Movie, HashMap<Movie, Integer>>();
    
    /**
     * Empty constructor for testing purposes
     * 
     */
    public MovieGraph(){	
    }
    	
	/**
	 * Add a new movie to the graph. If the movie already exists in the graph
	 * then this method will return false. Otherwise this method will add the
	 * movie to the graph and return true.
	 * 
	 * @param movie
	 *            the movie to add to the graph. Requires that movie != null.
	 * @return true if the movie was successfully added and false otherwise.
	 * @modifies this by adding the movie to the graph if the movie did not
	 *           exist in the graph.
	 */
	public boolean addVertex(Movie movie) {
		boolean wasAdded = false;
		//Check to see if the movie has been added yet
		if(!movies.containsKey(movie)) {
			movies.put(movie, new HashMap<Movie, Integer>());
			movieMap.put(movie.getName(), movie.getid());
			IDMap.put(movie.getid(), movie);
		    wasAdded = true;
		}
		
		return wasAdded;
	}

	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method will return false. Otherwise this method will add the edge to
	 * the graph and return true.
	 * 
	 * @param movie1
	 *            one end of the edge being added. Requires that m1 != null.
	 * @param movie2
	 *            the other end of the edge being added. Requires that m2 !=
	 *            null. Also require that m1 is not equal to m2 because
	 *            self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(Movie movie1, Movie movie2, int edgeWeight) {
		boolean wasAdded = false;
		//check to see if the an edge has already been made between the movies
		if(movies.containsKey(movie1) && movies.get(movie1).containsKey(movie2))
			return wasAdded;
		//check to see if the vertices have been initialized.
		if(movies.containsKey(movie1) && movies.containsKey(movie2)){
			movies.get(movie1).put(movie2, edgeWeight);
			movies.get(movie2).put(movie1, edgeWeight);
			wasAdded = true;
		}
		
		return wasAdded;
	}

	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method should return false. Otherwise this method should add the
	 * edge to the graph and return true.
	 * 
	 * @param movieId1
	 *            the movie id for one end of the edge being added. Requires
	 *            that m1 != null.
	 * @param movieId2
	 *            the movie id for the other end of the edge being added.
	 *            Requires that m2 != null. Also require that m1 is not equal to
	 *            m2 because self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(int movieId1, int movieId2, int edgeWeight) {
		Movie movie1 = IDMap.get(movieId1);
		Movie movie2 = IDMap.get(movieId2);
		
		return addEdge(movie1, movie2, edgeWeight);
	}
	
	
	/**
	 * Return the length of the shortest path between two movies in the graph.
	 * Throws an exception if the movie ids do not represent valid vertices in
	 * the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the length of the shortest path between the two movies
	 *         represented by their movie ids.
	 */
	public int getShortestPathLength(int movieId1, int movieId2)
			throws NoSuchMovieException, NoPathException {
		return getPathLength(getShortestPath(movieId1, movieId2));
	}
	private int getPathLength(List<Movie> path){
		int length = 0;
		Movie current, next;
		ListIterator<Movie> li = path.listIterator();
		
		current = li.next();
		while(li.hasNext()){
			next = li.next();
			
			length += movies.get(current).get(next);
			current = next;
		}
		
		return length;
	}

	/**
	 * Return the shortest path between two movies in the graph. Throws an
	 * exception if the movie ids do not represent valid vertices in the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the shortest path, as a list, between the two movies represented
	 *         by their movie ids. This path begins at the movie represented by
	 *         movieId1 and ends with the movie represented by movieId2.
	 */
	public List<Movie> getShortestPath(int movieId1, int movieId2)
			throws NoSuchMovieException, NoPathException {
		Map<Movie, Integer> distance = new HashMap<Movie, Integer>();
		Map<Movie, Movie> previous = new HashMap<Movie, Movie>();
		ArrayList<Movie> unvisited = new ArrayList<Movie>();
		
		Movie source, target;
		
		if(IDMap.containsKey(movieId1) && IDMap.containsKey(movieId2)){
			source = IDMap.get(movieId1); //start at movie 1
			target = IDMap.get(movieId2); //end at movie 2
		}
		else
			throw new NoSuchMovieException();
				
		distance.put(source, 0);
		
		for(Movie m : movies.keySet()){
			if(!m.equals(source)){
				distance.put(m, Integer.MAX_VALUE);
				previous.put(m, null);
			}
			unvisited.add(m);
		}
		
		while(!unvisited.isEmpty()){
			Movie curMovie = null;
			
			int shortDist = Integer.MAX_VALUE;
			for(Movie m : unvisited){  //Finds movie with smallest distance
				if(distance.get(m) < shortDist){
					shortDist = distance.get(m);
					curMovie = m;
				}
			}
			
			unvisited.remove(curMovie); //current movie has now been visited
			
			if(curMovie == null) //No movie with distance less than INFINITY in unvisited
				break;           //Indicates current movie is not connected to anything
			
			if(curMovie.equals(target))
				return pathToSource(previous, curMovie);
			
			for(Movie m : movies.get(curMovie).keySet()){ //for each of u's neighbours
				int alt = distance.get(curMovie) + movies.get(curMovie).get(m); //distance from source to u
															                    //added to dissimilarity between u and m
				if(alt < distance.get(m)){  
					distance.put(m, alt);
					previous.put(m, curMovie);
				}
			}
		}
		
		throw new NoPathException();
	}
	private List<Movie> pathToSource(Map<Movie, Movie> prev, Movie current){
		List<Movie> seq = new ArrayList<Movie>();
		Movie c = current;
		
		while(c != null){
			seq.add(c);
			c = prev.get(c);
		}
		
		return seq;
	}
	

	/**
	 * Returns the movie id given the name of the movie. For movies that are not
	 * in English, the name contains the English transliteration original name
	 * and the English translation. A match is found if any one of the two
	 * variations is provided as input. Typically the name string has <English
	 * Translation> (<English Transliteration>) for movies that are not in
	 * English.
	 * 
	 * @param name
	 *            the movie name for the movie whose id is needed.
	 * @return the id for the movie corresponding to the name. If an exact match
	 *         is not found then return the id for the movie with the best match
	 *         in terms of translation/transliteration of the movie name.
	 * @throws NoSuchMovieException
	 *             if the name does not match any movie in the graph.
	 */
	public int getMovieId(String name) throws NoSuchMovieException {
		
		if(!movieMap.containsKey(name))
			throw new NoSuchMovieException();
		else
			return movieMap.get(name);
	}
	
	/**
	 * Returns the movie with a given ID
	 */
	public Movie getMovie(int id){
		return IDMap.get(id);
	}

	// Implement the next two methods for completeness of the MovieGraph ADT

	@Override
	public boolean equals(Object other) {
		
		if(other.hashCode() == this.hashCode())
			return true; //We developed hash code to be robust
						 //a false positive is *very* unlikely
						 //and not worth considering
		return false;
	}

	@Override
	public int hashCode() {
	
		int x = 0;
		for(Movie m : IDMap.values())
			for(Integer i : movies.get(m).values())
				x += i;
		
		return movies.keySet().size() * x; //the product of number of movies and sum of all edge weights
	}
	
   /**
    * Getter for testing purposes
    * @return a set of the vertices
    */
   public Set<Movie> getVertecies(){
	   Set<Movie> vertices = new HashSet<Movie>();
	   vertices = movies.keySet();
	return vertices;
   }
   /**
    * Getter meathod for testing 
    * 
    * @param movie1 - first movie
    * @param movie2 - second movie
    * @return the weight between the two movies
    */
   public int getWeight(Movie movie1, Movie movie2){
	   int weight = movies.get(movie1).get(movie2).intValue();
	   return weight;
   }
}
